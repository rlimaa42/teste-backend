const mongoose = require("mongoose");

const voteSchema = new mongoose.Schema(
  {
    rate: {
      type: Number,
      required: true
    },
    movie: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Movie"
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Vote", voteSchema)