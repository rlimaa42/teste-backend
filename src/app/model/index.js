module.exports = {
  User: require("./user.model"),
  Movie: require("./movie.model"),
  Vote: require("./vote.model")
};