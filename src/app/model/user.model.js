const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const config = require("../../../config/index");

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    userName: {
      type: String,
      unique: true,
      required: true
    },
    pwd: {
      type: String,
      required: true
    },
    token: { 
      type: String 
    },
    isAdmin: {
      type: Boolean,
      required: true
    },
    isActive: {
      type: Boolean,
      required: true
    }
  },
  {
    timestamps: true
  }
);

userSchema.pre("save", function (next) {
  const user = this

  if (this.isModified("pwd") || this.isNew) {
    bcrypt.genSalt(config.saltRounds, function (saltError, salt) {
      if (saltError) {
        return next(saltError)
      } 
      else {
        bcrypt.hash(user.pwd, salt, function(hashError, hash) {
          if (hashError) {
            return next(hashError)
          }

          user.pwd = hash
          next()
        })
      }
    })
  } 
  else {
    return next()
  }
})


userSchema.pre("findOneAndUpdate", async function (next) {
  const user = this
  const docToUpdate = await this.model.findOne(this.getQuery())

  if (this._update.pwd !== docToUpdate.pwd) {
    bcrypt.genSalt(config.saltRounds, (saltError, salt) => {
      if (saltError) {
        return next(saltError)
      } 
      else {
        bcrypt.hash(user._update.pwd, salt,(hashError, hash) => {
          if (hashError) {
            return next(hashError)
          }

          user._update.pwd = hash
          next()
        })
      }
    })
  } 
  else {
    return next()
  }
})

module.exports = mongoose.model("User", userSchema)