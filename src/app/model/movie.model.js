const mongoose = require("mongoose");

const movieSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    director: {
      type: String,
      required: false
    },
    genre: {
      type: String,
      required: false
    },
    actors: {
      type: Array,
      required: false
    },
    votes: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Vote"
    }]

  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Movie", movieSchema)