const db = require('../model/index.js');
const jwt = require('jsonwebtoken');
const mongodb = require('mongodb');
const config = require("../../../config/index");

exports.register = async (req, res) => {
  const userData = req.body;
  const validation = validate(userData);
  if(validation.valid) {
    const existing = await db.User.findOne({userName: userData.userName});
    if(existing) {
      return res.status(409).send("User existente.");
    }

    const user = await db.User.create(userData);

    const token = jwt.sign(
      { user_id: user._id, userName: user.userName },
      config.tokenKey,
      {
        expiresIn: "2h",
      }
    );
    // save user token
    user.token = token;

  
    try {
      res.status(201).send(user);
    }
    catch(err){ 
      res.status(500).send({
          message: err.message || "Ocorreu um erro ao criar User."
      });
    }
  }
  else {
    let msg = '';
    validation.error.forEach(err => {
      msg += err + ' '
    })
    return res.status(400).send({
      message: msg
    });
  }
}

exports.login = (req, res) => {

}

exports.logout = (req, res) => {

}

validate = user => {
  let validation = {
    valid: true,
    error: []
  };

  if(!user.name) {
    validation.valid = false;
    validation.error.push('"Nome" do Usuário inválido!');
  }
  if(!user.userName) {
    validation.valid = false;
    validation.error.push('"Nome de Usuário" do Usuário inválido!');
  }
  if(!user.pwd) {
    validation.valid = false;
    validation.error.push('"Senha" do Usuário inválida!');
  }

  return validation;
}