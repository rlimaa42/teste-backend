const db = require('../model/index.js')
const mongodb = require('mongodb');

// exports.create = async (req, res) => {
//   const userData = req.body;
//   const validation = validate(userData);
//   if(validation.valid) {

//     const user = new db.User(userData);
  
//     try {
//       let data = await user.save();
//       res.send(data);
//     }
//     catch(err){ 
//       res.status(500).send({
//           message: err.message || "Ocorreu um erro ao criar Usuário."
//       });
//     }
//   }
//   else {
//     let msg = '';
//     validation.error.forEach(err => {
//       msg += err + ' '
//     })
//     return res.status(400).send({
//       message: msg
//     });
//   }
// };

exports.findAll = async (req, res) => {
  try {
    let users = await db.User.find();
    res.send(users);
  }
  catch(err) {
    res.status(500).send({
        message: err.message || "Ocorreu um erro ao buscar os Usuários."
    });
  }
};

exports.findOne = async (req, res) => {
  let user = await db.User.findById(req.params.userId);
  try {
    if(!user) {
      res.status(404).send({
          message: `Usuário com id ${req.params.userId} não encontrado`
      });            
      return;
    }
    res.send(user);
  }
  catch(err) {
    if(err.kind === 'ObjectId') {
      res.status(404).send({
        message: `Usuário com id ${req.params.userId} não encontrado!`
      });                
      return
    }
    res.status(500).send({
      message: `Error ao buscar usuário com id ${req.params.userId}`
    });
  }
};


exports.update = async (req, res) => {
  const userData = req.body;
  const validation = validate(userData);
  if(validation.valid) {
    let user = userData;
    try {
      let data = await db.User.findByIdAndUpdate(req.params.userId, user, {new: true})
      if(!data) {
        res.status(404).send({
          message: `Usuário com id ${req.params.userId} não encontrado.`
        });
        return
      }
      res.send(data);
    }
    catch(err) {
      if(err.kind === 'ObjectId') {
        res.status(404).send({
          message: `Usuário com id ${req.params.userId} não foi encontrado.`
        });                
        return
      }
      res.status(500).send({
        message: `Error ao atualizar usuário com id ${req.params.userId}`
      });
    }
  }
  else {
    res.status(400).send({
      message: validation.error
    });
  }
};

exports.delete = async (req, res) => {
  try {
    const userId = new mongodb.ObjectId(req.params.userId.toString().trim())
    let data = db.User.findByIdAndDelete(userId);
    if(!data) {
      return res.status(404).send({
          message: `Usuário com id ${req.params.userId} não encontrado`
      });
    }
    res.send({message: "Usuário excluído com sucesso!"});
  }
  catch(err) {
    if(err.kind === 'ObjectId' || err.name === 'NotFound') {
      res.status(404).send({
        message: `Usuário com id ${req.params.userId} não encontrado`
      });                
      return
    }
    res.status(500).send({
      message: `Não foi possível excluir usuário com id ${req.params.userId}`
    });
  }
};

exports.deactivate = async (req, res) => {
  try {
    const userId = req.params.userId.toString().trim();
    let data = await db.User.findByIdAndUpdate(userId, {isActive: false}, {new: true})
    return res.status(200).send({ message: `User desativado`})
  }
  catch(err)
  {
    return res.status(500).send({ message: `Erro ao desativar User`})
  }
}

exports.activate = async (req, res) => {
  try {
    const userId = req.params.userId.toString().trim();
    let data = await db.User.findByIdAndUpdate(userId, {isActive: true}, {new: true})
    return res.status(200).send({ message: `User ativado`})
  }
  catch(err)
  {
    return res.status(500).send({ message: `Erro ao ativar User`})
  }
}


validate = user => {
  let validation = {
    valid: true,
    error: []
  };

  if(!user.name) {
    validation.valid = false;
    validation.error.push('"Nome" do Usuário inválido!');
  }
  if(!user.userName) {
    validation.valid = false;
    validation.error.push('"Nome de Usuário" do Usuário inválido!');
  }
  if(!user.pwd) {
    validation.valid = false;
    validation.error.push('"Senha" do Usuário inválida!');
  }

  return validation;
}