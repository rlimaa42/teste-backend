const db = require('../model/index.js')
const mongodb = require('mongodb');

exports.create = async (req, res) => {
  const movieData = req.body;
  const validation = validate(movieData);
  if(validation.valid) {

    const movie = new db.Movie(movieData);
  
    try {
      let data = await movie.save();
      res.send(data);
    }
    catch(err){ 
      res.status(500).send({
          message: err.message || "Ocorreu um erro ao criar filme."
      });
    }
  }
  else {
    let msg = '';
    validation.error.forEach(err => {
      msg += err + ' '
    })
    return res.status(400).send({
      message: msg
    });
  }
};

exports.findAll = async (req, res) => {
  try {
    let movies = await db.Movie.find().populate("votes");

    res.send(movies);
  }
  catch(err) {
    res.status(500).send({
        message: err.message || "Ocorreu um erro ao buscar os Movies."
    });
  }
};

exports.findOne = async (req, res) => {
  let movie = await db.Movie.findById(req.params.movieId).populate("votes");
  try {
    if(!movie) {
      res.status(404).send({
          message: `filme com id ${req.params.movieId} não encontrado`
      });            
      return;
    }
    res.send(movie);
  }
  catch(err) {
    if(err.kind === 'ObjectId') {
      res.status(404).send({
        message: `filme com id ${req.params.movieId} não encontrado!`
      });                
      return
    }
    res.status(500).send({
      message: `Error ao buscar filme com id ${req.params.movieId}`
    });
  }
};


exports.update = async (req, res) => {
  const movieData = req.body;
  const validation = validate(movieData);
  if(validation.valid) {
    let movie = movieData;
    try {
      let data = await db.Movie.findByIdAndUpdate(req.params.movieId, movie, {new: true})
      if(!data) {
        res.status(404).send({
          message: `filme com id ${req.params.movieId} não encontrado.`
        });
        return
      }
      res.send(data);
    }
    catch(err) {
      if(err.kind === 'ObjectId') {
        res.status(404).send({
          message: `filme com id ${req.params.movieId} não foi encontrado.`
        });                
        return
      }
      res.status(500).send({
        message: "Erro ao atualizr filme com id " + req.params.movieId
      });
    }
  }
  else {
    res.status(400).send({
      message: validation.error
    });
  }
};

exports.delete = async (req, res) => {
  try {
    const movieId = new mongodb.ObjectId(req.params.movieId.toString().trim())
    let data = db.Movie.findByIdAndDelete(movieId);
    if(!data) {
      return res.status(404).send({
          message: `filme com id ${req.params.movieId} não encontrado`
      });
    }
    res.send({message: `Movie excluído com sucesso!`});
  }
  catch(err) {
    if(err.kind === 'ObjectId' || err.name === 'NotFound') {
      res.status(404).send({
        message: `Movie com id ${req.params.movieId} não encontrado`
      });                
      return
    }
    res.status(500).send({
      message: `Não foi possível excluir o Filme com id ${req.params.movieId}`
    });
  }
};

validate = movie => {
  let validation = {
    valid: true,
    error: []
  };

  if(!movie.name) {
    validation.valid = false;
    validation.error.push('"Nome" do filme inválido!');
  }

  return validation;
}   