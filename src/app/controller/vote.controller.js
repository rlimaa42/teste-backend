const db = require('../model/index.js')
const mongodb = require('mongodb');

exports.createVote = async (req, res) => {
  const voteData = req.body;
  const validation = validate(voteData);
  if(validation.valid) {

    const vote = await db.Vote.create(voteData);
  
    try {
        db.Movie.findByIdAndUpdate(voteData.movie,
            { $push: { votes: vote._id } },
            { new: true, useFindAndModify: false }
        );
        res.send(vote);
    }
    catch(err){ 
      res.status(500).send({
        message: err.message || "Ocorreu um erro ao criar voto."
      });
    }
  }
  else {
    let msg = '';
    validation.error.forEach(err => {
      msg += err + ' '
    })
    return res.status(400).send({
      message: msg
    });
  }
};

// exports.findAll = async (req, res) => {
//   try {
//     let votes = await db.Vote.find();

//     res.send(votes);
//   }
//   catch(err) {
//     res.status(500).send({
//         message: err.message || "Ocorreu um erro ao buscar os Votes."
//     });
//   }
// };

// exports.findOne = async (req, res) => {
//   let vote = await db.Vote.findById(req.params.voteId);
//   try {
//     if(!vote) {
//       res.status(404).send({
//           message: `voto com id ${req.params.voteId} não encontrado`
//       });            
//       return;
//     }
//     res.send(vote);
//   }
//   catch(err) {
//     if(err.kind === 'ObjectId') {
//       res.status(404).send({
//         message: `voto com id ${req.params.voteId} não encontrado!`
//       });                
//       return
//     }
//     res.status(500).send({
//       message: `Error ao buscar voto com id ${req.params.voteId}`
//     });
//   }
// };


// exports.update = async (req, res) => {
//   const voteData = req.body;
//   const validation = validate(voteData);
//   if(validation.valid) {
//     let vote = voteData;
//     try {
//       let data = await db.Vote.findByIdAndUpdate(req.params.voteId, vote, {new: true})
//       if(!data) {
//         res.status(404).send({
//           message: `voto com id ${req.params.voteId} não encontrado.`
//         });
//         return
//       }
//       res.send(data);
//     }
//     catch(err) {
//       if(err.kind === 'ObjectId') {
//         res.status(404).send({
//           message: `voto com id ${req.params.voteId} não foi encontrado.`
//         });                
//         return
//       }
//       res.status(500).send({
//         message: "Erro ao atualizr voto com id " + req.params.voteId
//       });
//     }
//   }
//   else {
//     res.status(400).send({
//       message: validation.error
//     });
//   }
// };

// exports.delete = async (req, res) => {
//   try {
//     const voteId = new mongodb.ObjectId(req.params.voteId.toString().trim())
//     let data = db.Vote.findByIdAndDelete(voteId);
//     if(!data) {
//       return res.status(404).send({
//           message: `voto com id ${req.params.voteId} não encontrado`
//       });
//     }
//     res.send({message: `Vote excluído com sucesso!`});
//   }
//   catch(err) {
//     if(err.kind === 'ObjectId' || err.name === 'NotFound') {
//       res.status(404).send({
//         message: `Vote com id ${req.params.voteId} não encontrado`
//       });                
//       return
//     }
//     res.status(500).send({
//       message: `Não foi possível excluir o voto com id ${req.params.voteId}`
//     });
//   }
// };

validate = vote => {
  let validation = {
    valid: true,
    error: []
  };

  if(!vote.rate) {
    validation.valid = false;
    validation.error.push('"Nota" do voto inválido!');
  }
  if(!vote.movie) {
    validation.valid = false;
    validation.error.push('"Filme" do voto inválido!');
  }

  return validation;
}   