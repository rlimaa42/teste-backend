module.exports = (app) => {
  const auth = require('../controller/auth.controller.js');

  app.post('/register', auth.register);

  app.post('/login', auth.login);

  app.post('/logout', auth.logout);
}