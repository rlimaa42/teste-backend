module.exports = (server) => {
  require('./user.routes.js')(server);
  require('./movie.routes.js')(server);
  require('./vote.routes.js')(server);
  require('./auth.routes.js')(server);
}