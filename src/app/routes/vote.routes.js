module.exports = (app) => {
  const vote = require('../controller/vote.controller.js');

  app.post('/vote', vote.createVote);

  // app.get('/vote', vote.findAll);

  // app.get('/vote/:voteId', vote.findOne);

  // app.put('/vote/:voteId', vote.update);

  // app.delete('/vote/:voteId', vote.delete);
}