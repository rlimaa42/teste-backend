module.exports = (app) => {
  const user = require('../controller/user.controller.js');

  // app.post('/user', user.create);

  app.post('/user/deactivate/:adminId', user.deactivate);

  app.post('/user/activate/:adminId', user.activate);

  app.get('/user', user.findAll);

  app.get('/user/:userId', user.findOne);

  app.put('/user/:userId', user.update);

  app.delete('/user/:userId', user.delete);
}