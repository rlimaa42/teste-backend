const config = require("../config");
const express = require("express");
const mongoose = require("mongoose");
const routes = require("./app/routes/index.js");

class App {
  constructor() {
    this.express = express();
    this.express.use(express.json())
    this.express.use(express.urlencoded({ extended: true}))

    routes(this.express);
    this.database();

    this.express.listen(config.port, () =>
      console.log(`Server started on port ${config.port}! `)
    );
  }

  database() {
    mongoose.connect(config.dbUrl, { useNewUrlParser: true });
  }

  middlewares() {
    this.express.use(express.json());
  }

  routes() {
    this.express.use(require("./routes"));
  }
}
module.exports = new App().express;