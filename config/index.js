const config = {
  dbUrl: process.env.DBURL || "mongodb://127.0.0.1:27017/imdb_app",
  port: process.env.PORT || 3000,
  env: process.env.NODE_ENV || "development",
  saltRounds: process.env.SALT_ROUNDS || 10,
  tokenKey: "7lXbDlaGButFVbpjyAno"
};

module.exports = config;